/*
 * Author: Eun Suk Lee
 * Date: 1/27/2015
 * Email:el24336@email.vccs.edu
 * This program will change format of date into European format
 */

import javax.swing.JOptionPane;

public class Assignment1 
{
	public static void main(String[] agrs)
	{
		//Set normal format of date as String
		String d =
		JOptionPane.showInputDialog("This program will change format of date\nEnter date in the format month/day/year (xx/xx/xx)");
		
		//classify month,day,year from the input
		String month = d.substring(0,2);
		String day = d.substring(3, 5);
		String year = d.substring(6, 8);
		
		//set output in European format with classified month,day,year
		JOptionPane.showMessageDialog(null,"your date in European format is " + day + "."+month+"."+year);
	}
}
